﻿using System.Web;
using System.Web.Mvc;

namespace CS2450_SeanRappleyea
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}